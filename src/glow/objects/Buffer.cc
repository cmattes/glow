#include "Buffer.hh"

#include "glow/glow.hh"

#include <limits>
#include <cassert>

using namespace glow;

Buffer::Buffer(GLenum bufferType) : mType(bufferType)
{
    checkValidGLOW();

    mObjectName = std::numeric_limits<decltype(mObjectName)>::max();
    glGenBuffers(1, &mObjectName);
    assert(mObjectName != std::numeric_limits<decltype(mObjectName)>::max() && "No OpenGL Context?");
}

Buffer::~Buffer()
{
    checkValidGLOW();

    glDeleteBuffers(1, &mObjectName);
}
